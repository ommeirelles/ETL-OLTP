# -*- coding: cp1252 -*-
import datetime
import pypyodbc

def validadorData(data):
    try:
        datetime.datetime.strptime(data, '%d/%m/%Y')
        return True
    except ValueError:
        return False
    else:
        return False


def validador(cpf):
    cpf = cpf.replace(".", "").replace("-", "")
    while cpf.isdigit() == False or len(cpf) != 11:
        return False
    if not cpf_falso(cpf):
        return False
    if calculo_digitos(cpf, 9) == False:
        return False
    if calculo_digitos(cpf, 10) == True:
        return True
    else:
        return False


def calculo_digitos(n, digito):
    contador = 0
    z = (digito + 1)
    for x in n[0:digito]:
        x = int(x)
        mult = z * x
        contador += mult
        z -= 1
    resto = (contador % 11)
    if resto < 2:
        resto = 0
    else:
        resto = 11 - resto
    if resto != int(n[digito]):
        return False
    else:
        return True


def cpf_falso(n):
    if n == "11111111111" or n == "22222222222" or n == "33333333333":
        return False
    elif n == "44444444444" or n == "55555555555" or n == "66666666666":
        return False
    elif n == "77777777777" or n == "88888888888" or n == "99999999999":
        return False
    else:
        return True


def verificaSeCPFExiste(cpf, db):
    cursor = db.cursor().execute("select top(1) cpf from cliente where cpf = '{0}'".format(str(cpf)))
    if cursor.fetchone():
        cursor.close()
        return True
    else:
        cursor.close()
        return False


def verificaSeCEPExiste(cep,cpf,db):
    cursor = db.cursor().execute("select top(1) * from endereco where cep = '{0}' and cpf = '{1}'".format(cep,cpf))
    if cursor.fetchone():
        cursor.close()
        return True
    else:
        cursor.close()
        return False

def verificaSeEmailExiste(email,cpf,db):
    cursor = db.cursor().execute("select top(1) * from email where email = '{0}' and cpf = '{1}'".format(email,cpf))
    if cursor.fetchone():
        cursor.close()
        return True
    else:
        cursor.close()
        return False

def verificaSeDividaExiste(contrato,cpf,db):
    cursor = db.cursor().execute("select top(1) * from dividas where id_divida = '{0}' and cpf = '{1}'".format(contrato,cpf))
    if cursor.fetchone():
        cursor.close()
        return True
    else:
        cursor.close()
        return False

def connect():
    db_host = '10.10.10.108,30415'
    db_name = 'MONOGRAFIA'
    db_user = 'sa'
    db_password = 'sa@123'
    connection_string = 'Driver={ODBC Driver 11 for SQL Server};Server=' + db_host + \
        ';Database=' + db_name + ';UID=' + db_user + ';PWD=' + db_password + ';'

    db = pypyodbc.connect(connection_string)
    db.autocommit = True
    return db