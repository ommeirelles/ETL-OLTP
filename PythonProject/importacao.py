# -*- coding: cp1252 -*-
import re
from decimal import *
import pypyodbc
import datetime
import helper

db = helper.connect()

arq = open('./arq2.txt', 'r')
linhas = arq.readlines()
arq.close()
init = datetime.datetime.now()

for i in range(len(linhas)):
    print '{0}Processando a linha: {1}{2}'.format('-'*15,i,'-'*15)
    cpf = linhas[i][0:11].strip()
    nome = linhas[i][11:111].strip()
    nomePai = linhas[i][111:211].strip()
    nomeMae = linhas[i][211:311].strip()
    dataNasc = linhas[i][311:321].strip()
    email = linhas[i][321:421].strip()
    numLog = linhas[i][421:424].strip()
    cep = linhas[i][424:433].strip()
    log = linhas[i][433:533].strip()
    bairro = linhas[i][533:633].strip()
    cidade = linhas[i][633:733].strip()
    contrato = linhas[i][733:783].strip()
    valor = linhas[i][783:795].strip()
    dataCon = linhas[i][795:805].strip()
    valorAtu = linhas[i][805:817].strip()

    if (
        helper.validador(cpf) and
        re.match(r'^[a-zA-Z][\w\.\-]+@[a-zA-Z]+\.[a-zA-Z\.]+$', email.strip()) and
        re.match(r'^[a-zA-Z ]+$', nome.strip()) and
        helper.validadorData(dataNasc) and
        helper.validadorData(dataCon) and
        re.match(r'^[0-9]{5}-[0-9]{3}$', cep.strip()) and
        (contrato.strip() and len(contrato.strip()) <= 30) and
        re.match(r'^[0-9]*[1-9]{1}[0-9]*$',valor) and
        len(cidade.strip()) < 50 and
        len(bairro.strip()) < 50 and
        re.match(r'^[0-9]{3}$',numLog)
    ):

        try:
            if not helper.verificaSeCPFExiste(cpf, db):
                db.cursor().execute(
                    "insert into cliente (CPF,NOME,DT_NASC,NOME_PAI,NOME_MAE) values ('{0}','{1}',try_convert(date,'{2}',103),'{3}','{4}')"
                    .format(
                        cpf, nome, dataNasc, nomeMae, nomePai
                    )
                ).commit()

            if not (helper.verificaSeCEPExiste(cep,cpf,db)):
                db.cursor().execute(
                    "insert into endereco (CPF,CEP,LOGRADOURO,BAIRRO,CIDADE,NUMERO) values ('{0}','{1}','{2}','{3}','{4}','{5}')"
                    .format(
                        cpf, cep, log, bairro, cidade, numLog
                    )
                ).commit()

            if not (helper.verificaSeDividaExiste(contrato,cpf,db)):
                db.cursor().execute(
                    "insert into dividas (CPF,ID_DIVIDA,VALOR,DATA,VALOR_ATUALIZADO) values ('{0}','{1}',try_convert(decimal(12,2),'{2}'),try_convert(date,'{3}',103),try_convert(decimal(12,2),'{4}'))"
                    .format(
                        cpf, contrato, valor[0:10]+'.'+valor[10:12], dataCon, valorAtu[0:10]+'.'+valorAtu[10:12]
                    )
                ).commit()

            if not (helper.verificaSeEmailExiste(email,cpf,db)):
                db.cursor().execute(
                    "insert into email (CPF,EMAIL) values ('{0}','{1}')"
                    .format(
                        cpf, email
                    )
                ).commit()


        except Exception as e:
            print 'Erro ao processar a linha: {0}\nErro: {1}'.format(i,e.message)

    else:
        if not helper.validador(cpf):
            print 'cpf invalido {0}'.format(cpf)
        if not re.match(r'^[a-zA-Z][\w\.\-]+@[a-zA-Z]+\.[a-zA-Z\.]+$', email.strip()):
            print 'E-mail invalido: {0}'.format(email.strip())
        if not re.match(r'^[a-zA-Z ]+$', nome.strip()):
            print 'Nome invalido: {0}'.format(nome)
        if not helper.validadorData(dataNasc.strip()):
            print 'Data de nascimento fora de formato: {0}'.format(dataNasc)
        if not helper.validadorData(dataCon.strip()):
            print 'Data do contrato fora de formato: {0}'.format(dataCon)
        if not re.match(r'^[0-9]{5}-[0-9]{3}$', cep.strip()):
            print 'CEP invalido: "{0}"'.format(cep)
        if len(contrato.strip()) > 30:
            print 'contrato maior que 30 caracteres : "{0}"'.format(contrato.split())
        if not re.match(r'^[0-9]{3}$',numLog):
            print 'Numero do logradouro com erro: "{0}"'.format(numLog)
        if not re.match(r'^[0-9]*[1-9]{1}[0-9]*$',valor):
            print 'Valor com erro: {0}'.format(valor);


print ((datetime.datetime.now() - init).total_seconds())